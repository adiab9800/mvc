<?php

use app\core\Application;

class d0002_add_password_column_to_table_users{

    public function up()
    {
        $db =Application::$app->database;
        $db->pdo->exec("ALTER TABLE users ADD COLUMN password VARCHAR(512) NOT NULL" );
    }
    public function down()
    {
        $db =Application::$app->database;
        $db->pdo->exec("ALTER TABLE users DROP COLUMN password" );
    }
}