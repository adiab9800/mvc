<?php
namespace app\models;
use app\core\DBModel;
class user extends DBModel{

    public $firstName;
    public $lastName;
    public $email;
    public $password;
    public $status =0;
    public $confirm_password;
    public function tableName(): string
    {
        return 'users';
    }
    public function attributes(): array
    {
        return ["firstName","email" ,"password" ,"lastName" ,"status"];
    }
    public function labels()
    {
        return [
            "firstName" =>"First Name",
            "email"     =>"Email",
            "password"  =>"Password",
            "lastName"  =>"Last Name",
            "status"    =>"Status",
            "confirm_password"=> "Confirm Password"
        ];
    }
    public function rules():array
    {
        return [
            'firstName'         => [self::REQUIRED,[self::MIN,'min'=>8],[self::MAX,'max'=>20]],
            'lastName'          => [self::REQUIRED,[self::MIN,'min'=>8],[self::MAX,'max'=>20]],
            'email'             => [self::REQUIRED,self::EMAIL,[self::UNIQUE,'class'=>self::class]],
            'password'          => [self::REQUIRED,[self::MIN,'min'=>8],[self::MAX,'max'=>20]],
            'confirm_password'  => [self::REQUIRED,[self::MATCH,'match'=>'password']]
        ];
    }
    public function save()
    {
        $this->password= password_hash($this->password,PASSWORD_DEFAULT);
        return parent::save();
    }
}