<?php
namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\Request;

class SiteController extends Controller{
    public function home()
    {
        $params =array(
            'name' =>'ahmed'
        );

        return $this->render('home',$params);
    }
    public function contact()
    {
        return $this->render('contact');
    }
    public function submitContact(Request $request)
    {
        var_dump($request->getBody());
    }
}