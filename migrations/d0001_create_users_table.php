<?php

use app\core\Application;

class d0001_create_users_table{

    public function up()
    {
        $db =Application::$app->database;
        $db->pdo->exec("CREATE TABLE users (
            id          INT AUTO_INCREMENT PRIMARY KEY,
            email       VARCHAR(255) NOT NULL UNIQUE ,
            firstName   VARCHAR(255) NOT NULL ,
            lastName    VARCHAR(255) NOT NULL ,
            status      TINYINT DEFAULT 0,
            created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
        ) ENGINE=INNODB" );
    }
    public function down()
    {
        $db =Application::$app->database;
        $db->pdo->exec("DROP TABLE users");
    }
}