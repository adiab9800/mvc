<?php 
namespace app\core;

use PDO;

class Database{
    public $pdo;
    public function __construct($config)
    {
        $dsn =$config['dsn'] ?? '';
        $user =$config['user'] ?? '';
        $password =$config['password'] ?? '';
        $this->pdo = new PDO($dsn,$user,$password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    }
    public function applyMigrations()
    {
        $this->createMigrationsTable();
        $appliedMigrations = $this->getAppliedMigrations();
        $newMigrations =[];
        $files = scandir(Application::$ROOT_PATH."/migrations");
        $toApplyMigrations = array_diff($files,$appliedMigrations);
        foreach($toApplyMigrations as $migration)
        {
           if($migration === '.' || $migration === '..')
           {
               continue;
           }
           require_once Application::$ROOT_PATH.'/migrations/'.$migration;
           $className = pathinfo($migration,PATHINFO_FILENAME);
           $instance = new $className();
           $this->log("Applying Migration $migration".PHP_EOL) ;
           $instance->up();
           $this->log("Applied Migration $migration".PHP_EOL);
           $newMigrations[]=$migration;
           
        }
        if(!empty($newMigrations))
           {
            $this->saveMigrations($newMigrations);
           }
           else
           {
            $this->log("nothing to migrate !".PHP_EOL);
           }
    }
    public function createMigrationsTable()
    {
        $this->pdo->exec("CREATE TABLE IF NOT EXISTS migrations(
                id INT AUTO_INCREMENT PRIMARY KEY,
                migration VARCHAR(255),
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
        ) ENGINE=INNODB ; ");
    }
    public function getAppliedMigrations()
    {
        $statement = $this->pdo->prepare("SELECT migration FROM migrations");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_COLUMN);
    }
    public function saveMigrations(array $migrations)
    {
        $str =implode("," ,array_map(function($m) { return "('$m')";},$migrations)) ;  ////output ('filename','filename2');
        $statement =$this->pdo->prepare("INSERT INTO migrations (migration) VALUES $str");
        $statement->execute();
    }
    protected function log(string $message)
    {
        echo "[".date("Y-m-d h:i:sa")."] - " . $message ;
    }
    public static function prepare($sql)
    {
        return Application::$app->database->pdo->prepare($sql);
    }
}