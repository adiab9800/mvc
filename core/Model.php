<?php 

namespace app\core;

abstract class Model{
    public const REQUIRED ='required';
    public const EMAIL ='email';
    public const MIN ='min';
    public const MAX ='max';
    public const MATCH ='match';
    public const UNIQUE ='unique';
    public $errors=[];
    public function loadData($data)
    {
        foreach($data as $key=>$value)
        {
            if(property_exists($this,$key))
            {
                $this->{$key} =$value;
            }
        }
    }
    public function labels(){
        return [];  
    } 
    public function getLabel($attribute)
    {
        return $this->labels()[$attribute] ?? $attribute;
    }
    abstract function rules():array;

    public function validate()
    {
        foreach($this->rules() as $attribute=>$rules)
        {
            $value = $this->{$attribute};
            foreach($rules as $rule)
            {
                $ruleName = $rule;
                if(!is_string($ruleName))
                {
                 $ruleName =$rule[0];   
                }
                if($ruleName === self::REQUIRED && !$value)
                {
                    $this->addError($attribute,self::REQUIRED);
                }
                if($ruleName === self::EMAIL && !filter_var($value,FILTER_VALIDATE_EMAIL))
                {
                    $this->addError($attribute,self::EMAIL);
                }
                if($ruleName === self::MIN && strlen($value) < $rule['min'])
                {
                    $this->addError($attribute,self::MIN,$rule);
                }
                if($ruleName === self::MAX && strlen($value) > $rule['max'])
                {
                    $this->addError($attribute,self::MAX,$rule);
                }
                if($ruleName === self::MATCH && $value !== $this->{$rule['match']})
                {
                    $this->addError($attribute,self::MATCH,$rule);
                }
                if($ruleName === self::UNIQUE) 
                {
                    $className = $rule['class'];
                    $uniqueAttribute = $rule['attribute']?? $attribute;
                    $tableName = $className::tableName();
                    $statement = Application::$app->database->prepare("SELECT * FROM $tableName WHERE $uniqueAttribute = :attr");
                    $statement->bindValue(':attr',$value);
                    $statement->execute();
                    $record = $statement->fetchObject();
                    if($record)
                    {
                        $this->addError($attribute,self::UNIQUE,['field'=>$this->getLabel($attribute)]);
                    }
                }
                
            }
        }
        return (count($this->errors)>0)?false:true;
       
    }
    public function addError($attribute,$rule,$params=[])
    {
        $message = $this->errorMEssages()[$rule] ?? '';
       foreach($params as $key=>$value)
       {
          $message= str_replace("{{$key}}",$value,$message);
       }
        $this->errors[$attribute][]=$message;
    }
    public function errorMEssages()
    {
        return [
            self::REQUIRED=> 'This field is required',
            self::EMAIL=> 'This field must be valid email',
            self::MIN=> 'min length of the field must be {min}',
            self::MAX=> 'max length of the field must be {max}',
            self::MATCH=> 'This field must be the same as {match}',
            self::UNIQUE=> 'record with this {field} already exist',
        ];
    }
    public function hasError($attribute)
    {
        return $this->errors[$attribute]?? false;
    }
    public function getFirstError($attribute)
    {
        return $this->errors[$attribute][0] ?? false;
    }
}