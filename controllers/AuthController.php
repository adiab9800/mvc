<?php
namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\Request;
use app\models\user;

class AuthController extends Controller{
    public function __construct()
    {
        $this->setLayout('auth');
    }
    public function login()
    {
        return $this->render('login');
    }
    public function submitLogin(Request $request)
    {
        
        return 'submitting login';
        // return $this->render('login');
    }
    public function register()
    {
        $user =new user();
        return $this->render('register',[
            'model'=>$user
        ]);
    }
    public function submitRegister(Request $request)
    {
        $data = $request->getBody();
        $user =new user();
        $user->loadData($data);
        if($user->validate() && $user->save())
        {
            // return 'success';
            Application::$app->session->setFlash('success','Your Registration Done Successfully !');
            var_dump($_SESSION['flash_messages']);
            Application::$app->response->redirect("/");
        }
        return $this->render('register',[
            'model'=>$user
        ]);
    }
  
}