<?php
namespace app\core; 
class Application{
    public static $ROOT_PATH;
    public static $app;
    public $router;
    public $request;
    public $response;
    public $session;
    public $controller;
    public $database;
    public function __construct($rootPath,$config)
    {
        self::$ROOT_PATH =$rootPath;
        self::$app =$this;
        $this->request =new Request();
        $this->response =new Response();
        $this->router =new Router($this->request,$this->response);
        $this->database =new Database($config['db']);
        $this->session =new Session();
    }
    public function getController()
    {
        return $this->controller;
    }
    public function setController(Controller $controller)
    {
        $this->controller = $controller;
    }
    public function run()
    {
        echo $this->router->resolve();
    }
}